// Load components
var express = require('express');
var app = express();
var helmet = require('helmet');
var compression = require('compression');

// Load routes files
var campaigns = require('./routes/campaigns');

// Define app "uses"
app.use(helmet());
app.use(compression());

// Map root route
app.get('/', function (req, res) {
  res.send('Compra Solidária API is listening for requests! (' + res.statusCode + ')');
});

// CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");
  next();
});

// Map nested routes
app.use('/campaigns', campaigns);


// Define default error handler
function errorHandler(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  console.error(err.stack);
  res.status(500);
  res.send('Ooops! Something went wrong with Compra Solidária API (' + res.statusCode + ')');
}
app.use(errorHandler);

// Set NodeJS to listen
app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'));
