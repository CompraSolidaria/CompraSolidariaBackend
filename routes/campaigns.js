// Load components
var express = require('express');
var bodyParser = require('body-parser')
var fb = require('fb');
var mysql = require('mysql');
var jwt = require('express-jwt');
var router = express.Router();

if (!process.env.fb_app_id ||
  !process.env.fb_app_secret ||
  !process.env.auth0_app_secret ||
  !process.env.auth0_app_id ||
  !process.env.auth0_app_domain ||
  !process.env.db_host ||
  !process.env.db_user ||
  !process.env.db_pass ||
  !process.env.db_schema ||
  !process.env.db_connLimit) {
  throw new Error('Environment not correctly set. Please check your config');
}

// Define MySQL pool
var mysqlPool = mysql.createPool({
  host: process.env.db_host,
  user: process.env.db_user,
  password: process.env.db_pass,
  database: process.env.db_schema,
  connectionLimit: process.env.db_connLimit
});

// Set route to use components
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

// Used by Mobile
// Define Check FB Token function
checkFbToken = function (req, res, next) {
  if (!req.header("Authorization")) {
    next(new Error('No token provided!'));
  } else {
    fb.options({
      appId: process.env.fb_app_id,
      appSecret: process.env.fb_app_secret,
      accessToken: req.header("Authorization")
    });
    fb.api('me', function (res) {
      if (res && res.error) {
        next(new Error(res.error.message));
      }
      else {
        next();
      }
    });
  }
}

// Used by Campaign Manager
// Define Check Auth0 Token function
checkAuth0Token = jwt({
  // Dynamically provide a signing key based on the kid in the header and the singing keys provided by the JWKS endpoint.
  secret: process.env.auth0_app_secret,
  // Validate the audience and the issuer.
  audience: process.env.auth0_app_id,
  issuer: process.env.auth0_app_domain,
  algorithms: ['HS256']
});

// Used by Mobile and Campaign Manager
// Search specific campaign pictures
router.get('/:campaignId/pictures', function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'select p.banner, p.url' +
        ' from pictures p' +
        ' where p.campaign_id = ' + mysql.escape(req.params.campaignId) +
        ' order by p.banner desc;',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.json(rows);
          }
        });
    }
  });
});

// Used by Campaign Manager
// Save a campaign picture
router.post('/:campaignId/pictures', checkAuth0Token, function (req, res, next) {
  if (!req.body.url ||
	  !req.body.banner) {
    next(new Error('Invalid or incomplete data!'));
  } else {
    mysqlPool.getConnection(function (err, connection) {
      if (err) {
        next(err);
      } else {
        connection.query(
          'insert into pictures (campaign_id, url, banner)' +
          ' values (' + 
		  mysql.escape(req.params.campaignId) + ',' + 
		  mysql.escape(req.body.url) + ',' + 
		  mysql.escape(req.body.banner) + ');',
          function (err, rows) {
            if (err) {
              connection.release();
              next(err);
            } else {
              connection.release();
              res.status(200);
              res.send(true);
            }
          });
      }
    });
  }
});

// Used by Campaign Manager
// Update a campaign picture
router.patch('/:campaignId/pictures', checkAuth0Token, function (req, res, next) {
  if (!req.body.url ||
	  (req.body.banner != 0 && req.body.banner != 1) ||
	  !req.body.oldUrl) {
    next(new Error('Invalid or incomplete data!'));
  } else {
    mysqlPool.getConnection(function (err, connection) {
      if (err) {
        next(err);
      } else {
        connection.query(
          'update pictures set' +
		  ' url = ' + mysql.escape(req.body.url) + ',' +
		  ' banner = ' + mysql.escape(req.body.banner) + 
          ' where campaign_id = ' + mysql.escape(req.params.campaignId) +
		  ' and url = ' + mysql.escape(req.body.oldUrl) + ';',
          function (err, rows) {
            if (err) {
              connection.release();
              next(err);
            } else {
              connection.release();
              res.status(200);
              res.send(true);
            }
          });
      }
    });
  }
});

// Used by Campaign Manager
// Remove a campaign picture
router.delete('/:campaignId/pictures/:picUrl', checkAuth0Token, function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'delete from pictures' +
        ' where campaign_id = ' +  mysql.escape(req.params.campaignId) +
		' and url = ' + mysql.escape(req.params.picUrl) + ';',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.send(true);
          }
        });
    }
  });
});

// Used by Mobile
// Search votable campaigns
router.get('/search/votable', checkFbToken, function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'select c.id, c.name, c.description, IFNULL(temp_votes.votes, 0) as votes' +
        ' from campaigns c' +
        ' left join (' +
        ' select v.campaign_id, count(v.user_id) as votes' +
        ' from votes v' +
        ' group by v.campaign_id' +
        ') temp_votes' +
        ' on (c.id = temp_votes.campaign_id)' +
        ' where c.status = ' + mysql.escape('V') +
        ' order by votes desc;',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.json(rows);
          }
        });
    }
  });
});

// Used by Mobile
// Search ended campaigns by year/month (upvote sorted)
router.get('/search/ended/:year/:month', function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'select c.id, c.name, c.description, IFNULL(temp_votes.votes, 0) as votes' +
        ' from campaigns c' +
        ' left join (' +
        ' select v.campaign_id, count(v.user_id) as votes' +
        ' from votes v' +
        ' group by v.campaign_id' +
        ') temp_votes' +
        ' on (c.id = temp_votes.campaign_id)' +
        ' where c.id like ' + mysql.escape('%' + req.params.year + req.params.month + '%') +
        ' and c.status = ' + mysql.escape('R') +
        ' order by votes desc;',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.json(rows);
          }
        });
    }
  });
});

// Used by Campaign Manager
// Search all campaigns by field and value (name sorted)
router.get('/search/all/:field/:searchValue', function (req, res, next) {
  var campaigns = [];
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'select c.id, c.name, c.description, c.status, IFNULL(temp_votes.votes, 0) as votes' +
        ' from campaigns c' +
        ' left join (' +
        ' select v.campaign_id, count(v.user_id) as votes' +
        ' from votes v' +
        ' group by v.campaign_id' +
        ') temp_votes' +
        ' on (c.id = temp_votes.campaign_id)' +
        ' where c.' + connection.escapeId(req.params.field) + ' like ' + mysql.escape('%' + req.params.searchValue + '%') +
        ' order by c.name asc;',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
			for (var i = 0; i < rows.length; i++) {
				var campaign = {};
				campaign.id = rows[i].id;
				campaign.name = rows[i].name;
				campaign.status = rows[i].status;
				campaign.votes = rows[i].votes;
				campaign.description = rows[i].description;
				campaigns.push(campaign);
			}
            res.status(200);
            res.json(campaigns);
          }
        });
    }
  });
});

// Used by Mobile
// Vote for a campaign
router.post('/vote', checkFbToken, function (req, res, next) {
  if (!req.body.userId || !req.body.campaignId) {
    next(new Error('Invalid or incomplete data!'));
  } else {
    mysqlPool.getConnection(function (err, connection) {
      if (err) {
        next(err);
      } else {
        connection.query(
          'insert into votes (user_id, campaign_id)' +
          ' values (' + mysql.escape(req.body.userId) + ',' + mysql.escape(req.body.campaignId) + ');',
          function (err, rows) {
            if (err) {
              connection.release();
              next(err);
            } else {
              connection.release();
              res.status(200);
              res.send(true);
            }
          });
      }
    });
  }
});

// Used by Mobile
// Search for user vote on votable campaigns
router.get('/vote/:userId', checkFbToken, function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'select count(v.user_id) as vote' +
        ' from campaigns c' +
        ' straight_join votes v' +
        ' on c.id = v.campaign_id' +
        ' where c.status = ' + mysql.escape('V') +
        ' and v.user_id = ' + mysql.escape(req.params.userId) + ';',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.json(rows[0].vote);
          }
        });
    }
  });
});

// Used by Campaign Manager
// Create a campaign
router.post('/', checkAuth0Token, function (req, res, next) {
  if (!req.body.id || 
	  !req.body.name ||
	  !req.body.status ||
	  !req.body.description) {
    next(new Error('Invalid or incomplete data!'));
  } else {
    mysqlPool.getConnection(function (err, connection) {
      if (err) {
        next(err);
      } else {
        connection.query(
          'insert into campaigns (id, name, status, description)' +
          ' values (' + 
		  mysql.escape(req.body.id) + ',' + 
		  mysql.escape(req.body.name) + ',' +
		  mysql.escape(req.body.status) + ',' +
		  mysql.escape(req.body.description) + ');',
          function (err, rows) {
            if (err) {
              connection.release();
              next(err);
            } else {
              connection.release();
              res.status(200);
              res.send(true);
            }
          });
      }
    });
  }
});

// Used by Campaign Manager
// Update a campaign field
router.patch('/:campaignId/:field', checkAuth0Token, function (req, res, next) {
  var sql = '';
  
  function update(tempSql) {
    mysqlPool.getConnection(function (err, connection) {
      if (err) {
        next(err);
      } else {
        connection.query(
        'update campaigns set' +
    	  tempSql +
        ' where campaigns.id = ' + mysql.escape(req.params.campaignId) + ';',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.send(true);
          }
        });
      }
    });
  }
	
  if (req.params.field == 'name' && req.body.name) {
  	  sql = ' name = ' + mysql.escape(req.body.name);
	  update(sql);
  } else if (req.params.field == 'status' && req.body.status) {
  	  sql = ' status = ' + mysql.escape(req.body.status);
	  update(sql);
  } else if (req.params.field == 'description' && req.body.description) {
      sql = ' description = ' + mysql.escape(req.body.description);
	  update(sql);
  } else {
	  next(new Error('Invalid or incomplete data!'));
  }
});

// Used by Campaign Manager
// Delete campaign (by ID)
router.delete('/:campaignId', checkAuth0Token, function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'delete from campaigns' +
        ' where id = ' + mysql.escape(req.params.campaignId) + ';',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.send(true);
          }
        });
    }
  });
});

// Used by Campaign Manager
// Calculate a new campaign id
router.get('/free', function (req, res, next) {
  mysqlPool.getConnection(function (err, connection) {
    if (err) {
      next(err);
    } else {
      connection.query(
        'select ifnull((select concat(date_format(now(), ' + mysql.escape('%Y%m') + '), ' + mysql.escape('_') + ', substring_index(c.id, ' + mysql.escape('_') + ', -1) + 1)' +
        ' from campaigns c' +
        ' where c.id like concat(' + mysql.escape('%') + ', date_format(now(), ' + mysql.escape('%Y%m') + '), ' + mysql.escape('%') + ')' +
		' order by c.id desc' +
		' limit 1), concat(date_format(now(), ' + mysql.escape('%Y%m') + '), ' + mysql.escape('_') + ', 1)) as next' + ';',
        function (err, rows) {
          if (err) {
            connection.release();
            next(err);
          } else {
            connection.release();
            res.status(200);
            res.json(rows[0].next);
          }
        });
    }
  });
});

// Exports all routes
module.exports = router;
